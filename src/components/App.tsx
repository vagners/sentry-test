import { useState } from 'react'
import { captureException } from '@sentry/browser'

export const App = () => {
  const [isBroken, toggleBroken] = useState(false)

  const brokenEventHandler = () => {
    throw new Error('Exception from event handler')
  }

  const handledException = () => {
    try {
      throw new Error('Exception inside try/catch block')
    } catch (e) {
      captureException(e)
    }
  }

  if (isBroken) {
    throw new Error('Exception from rendering')
  }

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        width: '300px',
        height: '100px',
        margin: '50px auto'
      }}
    >
      <button onClick={brokenEventHandler}>Throw from event handler</button>
      <button onClick={handledException}>Throw with catch block</button>
      <button onClick={() => toggleBroken(prev => !prev)}>
        Throw from rendering
      </button>
    </div>
  )
}
